# Dockerfile template for building Ansible Docker images based
# on Alpine GNU/Linux

FROM alpine:$ALPINE_VERSION

RUN \
    apk --update \
        add bash git sudo python$PYTHON_VERSION py$PYTHON_VERSION-pip openssl ca-certificates libffi-dev sshpass openssh-client rsync patch && \
    apk add --virtual build-dependencies \
        python$PYTHON_VERSION-dev libffi-dev openssl-dev build-base rust cargo && \
    pip$PYTHON_VERSION install --break-system-packages --no-cache-dir --upgrade pip && \
    pip$PYTHON_VERSION install --break-system-packages --no-cache-dir 'ansible==$ANSIBLE_VERSION.*' boto jmespath ipaddress netaddr && \
    apk del build-dependencies && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts

CMD [ "ansible-playbook", "--version" ]

