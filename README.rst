container-ansible-ci
====================

.. _Docker: http://docker.com/
.. _Ansible: http://docs.ansible.com/
.. _`Gitlab CI`: https://about.gitlab.com/gitlab-ci/
.. _`Debian testing`: https://wiki.debian.org/DebianTesting

The project provides Docker_ images with Ansible_ installed:

* Alpine Linux (``alpine_3.20``)

  * Ansible 2.9 (``alpine_3.13-ansible_2.9``)
  * Ansible 2.10 (``alpine_3.13-ansible_2.10``)
  * Ansible 10 (``alpine_3.13-ansible_10``)
  * Ansible 11 (``alpine_3.13-ansible_11``)

Other images available in the Container registry are not maintained any more and may
be outdated. They may be removed without prior notice at any time in the future.

All images are build automatically using `Gitlab CI`_.
