#!/bin/bash
# Generate Dockerfiles for selected versions

# python_version -unused- alpine_version ansible_version [ansible_version] ..."
VERSIONS=(
    "3 0 3.20 2.9 2.10 10 11"
)

REGISTRY='registry.gitlab.com/tomaskadlec/container-ansible-ci'

STAGES=""

function extract() {
    echo "$1" | sed 's/[[:space:]]\+/ /g' | cut -d' ' -f $2
}

cat << -CI- > .gitlab-ci.yml
# Build Ansible Docker images using Gitlab CI
# File is generated automatically. Run ./generator.sh to recreate it.

image: docker:latest

variables:
    DOCKER_DRIVER: overlay

services:
    - docker:19.03.5-dind
#    - docker:dind

before_script:
    - docker login -u gitlab-ci-token -p \$CI_JOB_TOKEN registry.gitlab.com

# Build runtime configuration, ie. run the ./generate.sh script to create the
# required Dockerfiles
config:
    stage: config
    only:
        - master
    script:
        - apk --no-cache add bash gettext
        - bash ./generate.sh
    artifacts:
        untracked: true
        expire_in: 1 day

-CI-
for VERSION in "${VERSIONS[@]}"; do
    export PYTHON_VERSION=$(extract "$VERSION" 1)
    export ALPINE_VERSION=$(extract "$VERSION" 3)
    ANSIBLE_VERSIONS=$(extract "$VERSION" 4-)
    IFS=' ' read -r -a ANSIBLE_VERSIONS <<< "$ANSIBLE_VERSIONS"
    for ANSIBLE_VERSION in "${ANSIBLE_VERSIONS[@]}"; do
        STAGE="alpine_${ALPINE_VERSION/./_}"
        STAGES=$(echo -e "$STAGES\n    -   \"$STAGE\"")
        JOB="${STAGE}-ansible_${ANSIBLE_VERSION/./_}"
        TAG="alpine_${ALPINE_VERSION}-ansible_${ANSIBLE_VERSION}"
        DOCKERFILE="Dockerfile.$JOB"
        export ANSIBLE_VERSION
        echo "==> Building ansible-$ANSIBLE_VERSION alpine-$ALPINE_VERSION"
        cat Dockerfile.alpine.tpl | envsubst '$PYTHON_VERSION:$ALPINE_VERSION:$ANSIBLE_VERSION' > "$DOCKERFILE"
        cat >> .gitlab-ci.yml << -CI-
"$JOB":
    stage: "$STAGE"
    only:
        - master
    script:
        - docker build --file "$DOCKERFILE" --tag "${REGISTRY}:${TAG}" .
        - docker push "${REGISTRY}:${TAG}"

-CI-
    done
done

cat >> .gitlab-ci.yml << -CI-
stages:
    -   config
$(echo "$STAGES" | sort -u | sed '/^[[:space:]]*$/d')

-CI-

